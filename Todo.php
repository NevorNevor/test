<?php
class TodoQuery
{
    public function TodoGetAll()
    {
        require_once 'Connection.php';
        $pdo = new Connection();
        $result = $pdo->Connect()->query('SELECT name from todo');
        $getall = array();
        while ($row = $result->fetch()) {
            $getall[] = $row['name'];
        }
        return $getall;
    }
}