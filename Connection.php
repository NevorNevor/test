<?php
class Connection
{
    public function Connect():PDO
    {
        $dsn = 'mysql:host=localhost;dbname=testdb';
        $user = 'root';
        $pass = 'root';
        try
        {
            $link = new PDO($dsn, $user, $pass);
        }
        catch (PDOException $e)
        {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
        return $link;
    }
}
